# Requirements

- Verify that the android sdk is installed globally
  - Linux. Inside: `~/.bashrc`
  ```
  export ANDROID_HOME=/home/{your user}/Android/Sdk/
  export PATH=${PATH}:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools 
  ```
  - MAC. Inside: `~/.bash_profile`
  ```
  export ANDROID_HOME=$HOME/Library/Android/sdk
  export PATH=$ANDROID_HOME/platform-tools:$PATH
  export PATH=$ANDROID_HOME/tools:$PATH
  ```
- Verify that you have installed build tool version in Android Studio

# Installation

## Download
<p>First you need download the files or clone the repository</p>

```
git@gitlab.com:edgargrs/rn_boilerplate.git
```

## Configure enviroment
Modify url api inside  `src/config/index` according to your server

```
export const baseURL = __DEV__ ? 'http://your-local-dev-ip:8000/api/' : 'https://your-host-here/api/';
export const serverURL = __DEV__ ? 'http://your-local-dev-ip:8000/' : 'https://your-host-here/';
export const reactotronURL = 'your-local-dev-ip';
```

## Install dependencies
```
npm install
```

## Rename yout project
```
react-native-rename <newName> -b <bundleIdentifier> // For iOS use xcode
```

iOS only
```
cd ios && pod install
```


# Usage
## Run project
Open console inside project and run
```
yarn start --reset-cache
```
Open another console and run
```
react-native run-android
```

# License

[MIT license](https://opensource.org/licenses/MIT).
