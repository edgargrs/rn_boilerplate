// export default function findRouteNameFromNavigatorState({ routes }) {
  // let route = routes[routes.length - 1];
  // while (route.index !== undefined) route = route.routes[route.index];
  // return route.routeName;
// }

export default function findRouteNameFromNavigatorState({index, routes}){
  let route = routes[index];
  while (route.index !== undefined) route = route.routes[route.index];
  return route.routeName;
}
