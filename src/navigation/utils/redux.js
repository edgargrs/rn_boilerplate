import {
  createReactNavigationReduxMiddleware
} from "react-navigation-redux-helpers";

const middleware = createReactNavigationReduxMiddleware(
  state => state.nav
);

// const navigationPropConstructor = createNavigationPropConstructor("root");

export { middleware };