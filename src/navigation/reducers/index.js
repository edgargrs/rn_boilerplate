import AppNavigator from "navigation/navigators";

import { createNavigationReducer } from "react-navigation-redux-helpers";
export default createNavigationReducer(AppNavigator);


// const initialState = AppNavigator.router.getStateForAction(AppNavigator.router.getActionForPathAndParams('Splash'));

// const nav = (state = initialState, action) => {
//   const nextState = AppNavigator.router.getStateForAction(action, state);
//   return nextState || state;
// };

// export default nav;
