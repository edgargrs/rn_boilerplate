import React, { Component } from 'react';
import { createAppContainer } from 'react-navigation';
import * as screenNames from 'navigation/screen_names';
import createAnimatedSwitchNavigator from 'react-navigation-animated-switch';
import { createStackNavigator } from 'react-navigation-stack';
import { Transition } from 'react-native-reanimated';


import Splash from 'features/splash/containers';
import WelcomeNavigator from './welcome';
import HomeNavigator from './home';
import Update from 'features/update/components';

const RootStack = createAnimatedSwitchNavigator(
  {
    [screenNames.SPLASH]: {
      screen: Splash
    },
    [screenNames.WELCOME]: {
      screen: WelcomeNavigator
    },
    [screenNames.HOME]: {
      screen: HomeNavigator
    },
    [screenNames.UPDATE]: {
      screen: Update
    },
  },
  {
    initialRouteName: screenNames.SPLASH,
    headerMode: 'none',
    defaultNavigationOptions: {
      gesturesEnabled: false,
    },
    transition: (
      <Transition.Together>
        <Transition.Out
          type="slide-bottom"
          durationMs={400}
          interpolation="easeIn"
        />
        <Transition.In type="fade" durationMs={500} />
      </Transition.Together>
    ),
  }
);

const AppNavigator = createAppContainer(RootStack);

export default AppNavigator;
