import { createStackNavigator } from 'react-navigation-stack';
import * as screenNames from 'navigation/screen_names';

import Home from 'features/home/containers';
import Profile from 'features/profile/containers';


const HomeNavigator = createStackNavigator(
  {
    [screenNames.MAIN]: { screen: Home },
    [screenNames.PROFILE]: { screen: Profile },
  },
  {
    initialRouteName: screenNames.MAIN,
    defaultNavigationOptions: {
      gesturesEnabled: false,
      headerBackTitle: null,
    },
  }
);


export default HomeNavigator;
