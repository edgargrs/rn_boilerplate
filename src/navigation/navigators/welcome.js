import { createStackNavigator } from 'react-navigation-stack';
import * as screenNames from 'navigation/screen_names';

import Welcome from 'features/welcome/containers';
import Login from 'features/login/containers';
import Signup from 'features/signup/containers';
import Terms from 'features/terms/containers';


const WelcomeNavigator = createStackNavigator(
  {
    [screenNames.INIT]: { screen: Welcome },
    [screenNames.LOGIN]: { screen: Login },
    [screenNames.SIGNUP]: { screen: Signup },
    [screenNames.TERMS]: { screen: Terms },
  },
  {
    initialRouteName: screenNames.INIT,
  }
);


export default WelcomeNavigator;
