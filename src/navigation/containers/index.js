import React from 'react';
import { connect } from 'react-redux';
import { BackHandler } from 'react-native';
import { NavigationActions } from 'react-navigation';
import MainNavigator from '../navigators';
import findRouteNameFromNavigatorState from '../utils/findRouteNameFromNavigatorState';
import { createReduxContainer } from "react-navigation-redux-helpers";
import * as screenNames from '../screen_names';


const AppNavigator = createReduxContainer(MainNavigator);

class ReduxNavigation extends React.Component {

  constructor(props){
    super(props);
  }

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
  }

  onBackPress = () => {
    const { dispatch, nav } = this.props;
    if (nav.index === 0) {
      return false;
    }

    if (findRouteNameFromNavigatorState(nav) === screenNames.MAIN || findRouteNameFromNavigatorState(nav) == screenNames.INIT) {
      BackHandler.exitApp();
      return true;
    }

    dispatch(NavigationActions.back());
    return true;
  };

  render() {
    const { dispatch, nav} = this.props;
    return <AppNavigator state={nav} dispatch={dispatch} />;
  }
}

const mapStateToProps = state => ({
  nav: state.nav
});

export default connect(mapStateToProps)(ReduxNavigation);
