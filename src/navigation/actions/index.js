import { NavigationActions, StackActions } from 'react-navigation';
import * as screenNames from 'navigation/screen_names';

export const navigateToSplash = () =>
  NavigationActions.navigate({ routeName: screenNames.SPLASH });

  

  
export const navigateToWelcome = () =>
  NavigationActions.navigate({ routeName: screenNames.WELCOME });

  export const resetToWelcome = () =>
    StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: screenNames.INIT })]
    });
  
  export const navigateToLogin = () =>
    NavigationActions.navigate({ routeName: screenNames.LOGIN });

  export const navigateToSignup = () =>
    NavigationActions.navigate({ routeName: screenNames.SIGNUP });

  export const navigateToTerms = () =>
    NavigationActions.navigate({ routeName: screenNames.TERMS });




  export const navigateToHome = () =>
    NavigationActions.navigate({ routeName: screenNames.HOME });

  export const navigateToProfile = () =>
    NavigationActions.navigate({ routeName: screenNames.PROFILE });

  export const resetToHome = () =>
    StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: screenNames.HOME })]
    });
    



  export const navigateToUpdate = () =>
    NavigationActions.navigate({ routeName: screenNames.UPDATE });
