import validatejs from 'validate.js'; // http://validatejs.org/
const validation = {
    name:{
      presence: {
        allowEmpty: false,
        message: '^Ingresa tu nombre por favor!'
      }
    },
    phone:{
      presence: {
        allowEmpty: false,
        message: '^El número de teléfono es requerido!'
      },
      numericality: {
        onlyInteger: true,
        message: '^El teléfono solo debe incluir números'
      },
      length: {
        minimum: 10,
        message: '^El teléfono debe contener 10 números',
      },
    },
    email: {
      presence: {
        allowEmpty: false,
        message: '^Ingresa una dirección de correo electrónico',
      },
      email: {
        message: '^Ingresa un correo electrónico válido',
      },
    },

    password: {
      presence: {
        allowEmpty: false,
        message: '^Por favor ingresa la contraseña',
      },
      length: {
        minimum: 6,
        message: '^La contraseña debe tener al menos 6 caracteres',
      },
    },
  }

export default function(fieldName, value) {
    // Validate.js validates your values as an object
    // e.g. var form = {email: 'email@example.com'}

    // Create an object based on the field name and field value
    var formValues = {};
    formValues[fieldName] = value;

    // Create an temporary form with the validation fields
    // e.g. var formFields = {
    //                        email: {
    //                         presence: {
    //                          message: 'Email is blank'
    //                         }
    //                       }
    var formFields = {};
    formFields[fieldName] = validation[fieldName];

    // The formValues and validated against the formFields
    // the variable result hold the error messages of the field
    const result = validatejs(formValues, formFields);

      // If there is an error message, return it!
    if (result) {
        // Return only the field error message if there are multiple
        return result[fieldName][0]
    }
    return null
}