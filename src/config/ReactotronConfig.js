import Reactotron, { trackGlobalErrors } from 'reactotron-react-native';
import { reactotronRedux } from 'reactotron-redux';
import { reactotronURL } from 'config';

const reactotron = Reactotron.configure({ name: 'rn_politico', host: reactotronURL, port: 9090 })
    .use(reactotronRedux())
    .use(trackGlobalErrors())
    .useReactNative({ asyncStorage: false })
    .connect();
Reactotron.clear();
console.tron = reactotron;

export default reactotron;