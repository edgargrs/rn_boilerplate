import AsyncStorage from '@react-native-community/async-storage';
import { YellowBox } from 'react-native';

export const DebugConfig = {
    useFixtures: false,
    yellowBox: false,
    reduxLoggin: __DEV__,
    useReactotron: __DEV__
};

if (__DEV__) {
    // If ReactNative's yellow box warnings are too much, it is possible to turn
    // it off, but the healthier approach is to fix the warnings.
    YellowBox.ignoreWarnings(['Warning: ReactNative.createElement']);
    YellowBox.ignoreWarnings(['react-native BugReporting extraData']);
    console.disableYellowBox = !DebugConfig.yellowBox;
}

const IGNORE_ACTIONS = [
    // "persist/PERSIST",
    'persist/PURGE',
    'persist/REHYDRATE',
    'Navigation/NAVIGATE',
    'Navigation/COMPLETE_TRANSITION',
    'Navigation/BACK'
];

export const REDUX_PERSIST = {
    key: 'navigation',
    version: 1.0,
    storage: AsyncStorage,
    blacklist: ['nav'], // reducer keys that you do NOT want stored to persistence here
    whitelist: [''] //Optionally, just specify the keys you DO want stored to
};

export const baseURL = __DEV__ ? 'http://192.168.100.90:8000/api/' : 'https://your-host-here/api/';
export const serverURL = __DEV__ ? 'http://192.168.100.90:8000/' : 'https://your-host-here/';
export const reactotronURL = '192.168.100.90';