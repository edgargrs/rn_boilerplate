import { combineReducers } from "redux";
import nav from "navigation/reducers";
import splash from 'features/splash/reducers';

export default combineReducers({
  nav,
  splash,
});
