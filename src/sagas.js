import { takeLatest, put, fork } from 'redux-saga/effects';
import { STARTUP } from 'features/app/constants';
import splashSaga from 'features/splash/sagas';
import { checkNetworkState, checkAppVersion, checkLatestVersion} from 'features/splash/actions';

function* watchAppStart(action) {
  yield put(checkNetworkState());
  yield put(checkAppVersion());
  yield put(checkLatestVersion());
}

export default function* appSaga() {
  yield takeLatest(STARTUP, watchAppStart);
  yield fork(splashSaga);
}
