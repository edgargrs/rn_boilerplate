import axios from 'axios';
import { assign, get } from 'lodash';
import { getSystem } from 'features/splash/selectors';
import { store } from 'features/app';
// import config from './config';
// import { store } from 'features/App';
// import { getRefreshToken } from 'features/Auth/selectors';
// import { setAuthState, logout as logoutAction } from 'features/Auth/actions';

const baseURL = 'http://www.partytimepromos.com/api/';

const api = axios.create({
  baseURL,
  headers: {
    'Cache-Control': 'no-cache'
  },
  timeout: 20000
});

// handle unauthorized request
// const createInterceptor = () => {
//   this.responseInterceptor = api.interceptors.response.use(
//     response => response,
//     error => {
//       //console.tron.log('Interceptor ', error);
//       let errorResponse = error.response;
//       if (
//         errorResponse.status === 401 &&
//         !errorResponse.data.error === 'invalid_credentials'
//       ) {
//         api.interceptors.response.eject(this.responseInterceptor);
//         const refresh_token = getRefreshToken(store.getState());
//         const payload = { refresh_token, grant_type: 'refresh_token' };
//         return authenticate(payload)
//           .then(({ data }) => {
//             const token = get(data, 'access_token');
//             setAuthorizationHeader(token);
//             errorResponse.config.headers['Authorization'] = `Bearer ${token}`;
//             createInterceptor();
//             api(errorResponse.config);
//             store.dispatch(setAuthState(data));
//           })
//           .catch(error => {
//             store.dispatch(logoutAction());
//             return Promise.reject(error);
//           });
//       }
//       return Promise.reject(error);
//     }
//   );
// };

// createInterceptor();

// const setAuthorizationHeader = (token = null) => {
//   if (token) {
//     api.defaults.headers.common.authorization = `Bearer ${token}`;
//   } else {
//     delete api.defaults.headers.common.authorization;
//   }
// };

// const authenticate = payload =>
//   api.post('oauth/token', assign({}, config, payload));

// const getLanguages = () => api.get('languages');

// const getLanguageBundles = () => {
//   const mock = {
//     status: 200,
//     data: require('fixtures/language-bundles.json')
//   };
//
//   return Promise.resolve(mock);
// };

// const login = payload =>
//   authenticate(assign({}, payload, { grant_type: 'password' }));
//
// const resetPassword = payload => api.post('password/email', payload);

//

// const register = payload =>
//   api.post('register', assign({}, config, payload, { grant_type: 'password' }));

// const logout = () => api.post('user/logout');

// const getUser = access_token => api.get('user', access_token);
const getLatestVersion = () => api.post('checkVersion',{
    platform: getSystem(store.getState())
});

const getTest = () => api.get('test');
const setTest = () => api.post('test', {
  params: {
    'id': 1,
    'name': 'test',
    'pass': '123'
  }
});

export default {
  getTest,
  setTest,
  getLatestVersion
  // getLanguages,
  // getLanguageBundles,
  // login,
  // logout,
  // register,
  // resetPassword,
  // setAuthorizationHeader,
  // getUser,
};
