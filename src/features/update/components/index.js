import React from 'react';
import { View, Text} from 'react-native';
import styles from './styles';

class Update extends React.Component {

  constructor(props){
    super(props);
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>Update view view</Text>
      </View>
    );
  }
}

export default Update;
