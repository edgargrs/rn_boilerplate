import { connect } from 'react-redux';
import Component from 'features/terms/components';

const mapStateToProps = state => ({

});

const mapDispatchToProps = dispatch => ({
  
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Component);