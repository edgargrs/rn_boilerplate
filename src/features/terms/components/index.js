import React from 'react';
import { ScrollView, Text} from 'react-native';
import styles from './styles';

class Terms extends React.Component {

  constructor(props){
    super(props);
  }

  static navigationOptions = {
    title: "Terminos y Condiciones",
  };

  render() {
    return (
      <ScrollView contentContainerStyle={styles.container}>

        <Text>
          Aliqua labore Lorem amet ex ullamco adipisicing consectetur laborum enim. Nostrud sint ipsum nostrud consequat id est magna sint cillum voluptate cillum veniam laborum. Adipisicing sunt non commodo incididunt exercitation. Et laborum aliqua culpa sunt ex consectetur. Do adipisicing esse occaecat cillum deserunt non aliqua. Aliqua voluptate deserunt nisi Lorem consectetur eiusmod laboris ex. Eiusmod dolor aute nostrud officia aute minim do aliquip nostrud occaecat ad ipsum.
        </Text>

        <Text>
          Elit consequat ex magna reprehenderit veniam eiusmod id nulla sint ea sunt laboris Lorem. Aute ut nostrud ea nisi quis culpa nisi laboris. Eu irure do excepteur nostrud nostrud dolore ad incididunt cupidatat aliquip eu. Incididunt officia ullamco culpa enim ad laboris deserunt dolore cupidatat irure cupidatat cillum. Aliquip nulla sunt cupidatat duis ad cillum enim eiusmod qui ea voluptate deserunt officia eiusmod.
        </Text>

        <Text>
          Esse ullamco nulla enim reprehenderit aliquip aute id culpa consequat. Consectetur est do qui elit nulla. Eiusmod culpa ea ea nisi enim laborum quis. Ut aliquip commodo ea dolore laborum ipsum. Eu ut est elit amet sit consectetur ad velit reprehenderit cillum. Voluptate consequat minim laboris dolore cupidatat dolor duis culpa cupidatat nostrud. Occaecat excepteur minim sit ex mollit quis ut aute nulla aliqua ullamco.
        </Text>

        <Text>
          Ad do mollit ad dolore. Magna non anim id magna sint mollit. Id est exercitation id reprehenderit sint voluptate sit ad do occaecat eiusmod incididunt est nulla.
        </Text>

        <Text>
          Quis do sint aute commodo nisi consequat deserunt incididunt ad exercitation ipsum proident dolor cillum. Est reprehenderit veniam dolor et consequat. Non aute laboris nisi quis elit ullamco nostrud veniam aliquip nostrud do nisi aliqua. Occaecat cupidatat in labore adipisicing sit. Officia proident exercitation ut commodo esse nisi occaecat velit dolore sunt.
        </Text>

        <Text>
          Quis do sint aute commodo nisi consequat deserunt incididunt ad exercitation ipsum proident dolor cillum. Est reprehenderit veniam dolor et consequat. Non aute laboris nisi quis elit ullamco nostrud veniam aliquip nostrud do nisi aliqua. Occaecat cupidatat in labore adipisicing sit. Officia proident exercitation ut commodo esse nisi occaecat velit dolore sunt.
        </Text>

        <Text>
          Quis do sint aute commodo nisi consequat deserunt incididunt ad exercitation ipsum proident dolor cillum. Est reprehenderit veniam dolor et consequat. Non aute laboris nisi quis elit ullamco nostrud veniam aliquip nostrud do nisi aliqua. Occaecat cupidatat in labore adipisicing sit. Officia proident exercitation ut commodo esse nisi occaecat velit dolore sunt.
        </Text>

        <Text>
          Quis do sint aute commodo nisi consequat deserunt incididunt ad exercitation ipsum proident dolor cillum. Est reprehenderit veniam dolor et consequat. Non aute laboris nisi quis elit ullamco nostrud veniam aliquip nostrud do nisi aliqua. Occaecat cupidatat in labore adipisicing sit. Officia proident exercitation ut commodo esse nisi occaecat velit dolore sunt.
        </Text>

        <Text>
          Quis do sint aute commodo nisi consequat deserunt incididunt ad exercitation ipsum proident dolor cillum. Est reprehenderit veniam dolor et consequat. Non aute laboris nisi quis elit ullamco nostrud veniam aliquip nostrud do nisi aliqua. Occaecat cupidatat in labore adipisicing sit. Officia proident exercitation ut commodo esse nisi occaecat velit dolore sunt.
        </Text>
        
      </ScrollView>
    );
  }
}

export default Terms;
