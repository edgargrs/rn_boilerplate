import {
  LOADED_SPLASH,
  SET_CONNECTION,
  FAILED_CONNECTION,
  SET_SYSTEM,
  SET_VERSION,
  SET_LAST_VERSION,
  SET_FORCE_UPDATE,
 } from '../constants';
import { handleActions } from 'redux-actions';

const initialState = {
  showSplash: false,
  hasConnection: false,
  system: '',
  version: 1,
  last_version: 1,
  force_update: false
};

const splashReducer = handleActions({
    [LOADED_SPLASH]: (state, action) => ({
      ...state,
      showSplash: true
    }),
    [SET_CONNECTION]: (state, action) => ({
      ...state,
      hasConnection: action.payload
    }),
    [FAILED_CONNECTION]: (state, action) => ({
      ...state,
      hasConnection: false
    }),
    [SET_SYSTEM]: (state, action) => ({
      ...state,
      system: action.payload
    }),
    [SET_VERSION]: (state, action) => ({
      ...state,
      version: action.payload
    }),
    [SET_LAST_VERSION]: (state, action) => ({
      ...state,
      last_version: action.payload
    }),
    [SET_FORCE_UPDATE]: (state, action) => ({
      ...state,
      force_update: action.payload
    }),
  },
  initialState
);

export default splashReducer;
