import { createSelector } from 'reselect';
import { get } from 'lodash';

export const getSplashState = state => get(state, 'splash');

export const getHasShow = createSelector([getSplashState], splash =>
  get(splash, 'showSplash')
);

export const hasConnection = createSelector([getSplashState], splash =>
  get(splash, 'hasConnection')
);

export const getSystem = createSelector([getSplashState], splash =>
  get(splash, 'system')
);

export const getVersion = createSelector([getSplashState], splash =>
  get(splash, 'version')
);

export const getLatestVersion = createSelector([getSplashState], splash =>
  get(splash, 'last_version')
);

export const forceUpdate = createSelector([getSplashState], splash =>
  get(splash, 'force_update')
);