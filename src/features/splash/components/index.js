import React from 'react';
import { View, Text, ImageBackground, Image } from 'react-native'
import styles from './styles';
import Images from 'assets/images.js';

class Splash extends React.Component {

  constructor(props){
    super(props);
  }

  static navigationOptions = {
    headerMode: 'none',
    header: null,
  };

  animateSplash() {
    setTimeout(() => {
      this.props.handleWelcome();
    }, 3000);  }

  render() {
    this.animateSplash();
    return(
      <View style={styles.container}>
        <Text>Splash view</Text>
      </View>
    );
  }
}

export default Splash;
