import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  logoContainer:{
    width: 200,
    height: 200,
  },
  logo: {
    flex: 1,
    width: 150,
    height: 150,
    resizeMode: 'contain',    
  },
});
