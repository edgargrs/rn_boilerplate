import { all, takeLatest, call, put, select } from 'redux-saga/effects';
import { HANDLE_WELCOME } from '../constants';
import { 
    hasConnection, 
    getSystem,
    getVersion,
    getLatestVersion,
    forceUpdate 
} from 'features/splash/selectors';
import { navigateToWelcome, navigateToUpdate } from 'navigation/actions';
import Toast from 'react-native-simple-toast';


function* handleWelcome() {
  let connection = yield select(hasConnection);
//   let system = yield select(getSystem);
  let currentVersion = yield select(getVersion);
  let latestVersion = yield select(getLatestVersion);
  let mustUpdate = yield select(forceUpdate); // ¿Es necesaria la actualización?
//   console.log("Connection: "+connection);
//   console.log("System: "+system);
//   console.log('Version: '+currentVersion);
//   console.log('LatestVersion: '+latestVersion);
//   console.log('Must update: '+mustUpdate);
  if(connection){
    if(latestVersion > currentVersion && mustUpdate){
        yield put(navigateToUpdate());
    }else{
      yield put(navigateToWelcome());
    }
  }else{
    Toast.show("Necesitas internet para continuar");
  }

}

export default function*() {
  yield takeLatest(HANDLE_WELCOME, handleWelcome);
}
