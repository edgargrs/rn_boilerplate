import { createAction } from 'redux-actions';
import {
  HANDLE_WELCOME,
  LOADED_SPLASH,
  SET_CONNECTION,
  SET_SYSTEM,
  SET_VERSION,
  SET_LAST_VERSION,
  SET_FORCE_UPDATE,
} from '../constants';
import NetInfo from "@react-native-community/netinfo";
import api from 'services/api';
import DeviceInfo from 'react-native-device-info';
import { get } from 'lodash';



export const loadedSplash = createAction(LOADED_SPLASH);
export const handleWelcome = createAction(HANDLE_WELCOME);
export const setConnection = createAction(SET_CONNECTION);
export const setSystem = createAction(SET_SYSTEM);
export const setVersion = createAction(SET_VERSION);
export const setLastVersion = createAction(SET_LAST_VERSION);
export const setForceUpdate = createAction(SET_FORCE_UPDATE);



export const checkNetworkState = () => (dispatch, getState) => {
  return NetInfo.isConnected.fetch()
  .then((isConnected) => {
    dispatch(setConnection(isConnected))
  })
  .catch((error) => {
    console.log("checkNewtworkState error");
  });
};


export const checkAppVersion = () => (dispatch, getState) => {
  const systemName = DeviceInfo.getSystemName().toLowerCase();
  const version = DeviceInfo.getVersion();
  if (systemName && version) {
    dispatch(setSystem(systemName));
    dispatch(setVersion(version));
  }
};

export const checkLatestVersion = () => (dispatch, getState) => {
  return api.getLatestVersion().then((res) => {
    dispatch(setLastVersion(get(res, 'data.version')));
    dispatch(setForceUpdate(get(res, 'data.force_update')));
  }).catch((e) => {
    console.log(e);
  });
};
