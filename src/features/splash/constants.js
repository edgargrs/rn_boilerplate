export const HANDLE_WELCOME = 'app/HANDLE_WELCOME';
export const LOADED_SPLASH = 'app/LOADED_SPLASH';
export const SET_CONNECTION = 'app/SET_CONNECTION';
export const SET_SYSTEM = 'app/SET_SYSTEM';
export const SET_VERSION = 'app/SET_VERSION';
export const SET_LAST_VERSION = 'app/SET_LAST_VERSION';
export const SET_FORCE_UPDATE = 'app/SET_FORCE_UPDATE';
