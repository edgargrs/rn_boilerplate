import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Splash from '../components';
import {
  handleWelcome
  } from 'features/splash/actions';


const mapStateToProps = state => ({
  
});

const mapDispatchToProps = dispatch => bindActionCreators({ handleWelcome }, dispatch);


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Splash);