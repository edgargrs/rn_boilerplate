import React from 'react';
import { 
  View, 
  Text, 
  Button,
} from 'react-native';
import styles from './styles';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

class Signup extends React.Component {

  constructor(props){
    super(props);
  }

  state = {
    data: {
      name: '',
      email: '',
      phone: '',
      password: '',
    }
  }

  static navigationOptions = {
    title: "Registrarse",
    header: null,
  };

  onChange = name => value => {
    this.setState({
      data: { ...this.state.data, [name]: value }
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <Text>Sign up View</Text>
        <Button title="Sign up" onPress={() => this.props.navigateToHome() }/>
      </View>
    );
  }
}

export default Signup;
