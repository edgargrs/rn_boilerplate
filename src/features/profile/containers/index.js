import { connect } from 'react-redux';
import Component from 'features/profile/components';
import { bindActionCreators } from 'redux';
import {

} from 'navigation/actions';

const mapStateToProps = state => ({

});

const mapDispatchToProps = dispatch => bindActionCreators({ 

  }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Component);