import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    paddingVertical:10,
  },
  imageContainer:{
    height: 150,
    width: 150,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image:{
    width: '100%',
    height: '100%',
    borderRadius: 100,
  },
  dataContainer:{
    flex: 1,
    padding: 10
  },
  inputContainer:{
    flexDirection: 'row',
    width: '100%',
    height:40,
    borderWidth: 0.5,
    borderRadius: 5,
    marginVertical: 5,
    // backgroundColor: 'red',
  },
  label:{
    alignContent: 'center',
    width: '28%',
    alignSelf: 'center',
    textAlign: 'center',
    // backgroundColor: 'blue',
  },
  input:{
    flex: 1,
    backgroundColor: '#ECE8E8',
    borderRadius: 5,
    // backgroundColor: 'green',
    padding: 10,
  },
  hr: {
    borderBottomWidth: 1,
    borderBottomColor: 'black',
    width: '100%',
    paddingVertical: 2,
  },
  numRegister:{
    flex: 1,
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'center',
    // backgroundColor: 'red',
  },
  numRegisterHeader:{
    fontSize: 30,
  },
  numRegisterNumber:{
    fontSize: 80,
    fontWeight: 'bold',
    paddingBottom: 40
  },
});
