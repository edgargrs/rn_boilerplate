import React from 'react';
import { View, Text, TextInput, Image} from 'react-native';
import styles from './styles';

class Profile extends React.Component {

  constructor(props){
    super(props);
  }

  static navigationOptions = {
    title: "Perfil",
  };

  render() {
    return (
      <View style={styles.container}>
        <Text>Profile Screen</Text>
      </View>
    );
  }
}

export default Profile;
