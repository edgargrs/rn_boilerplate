import { StyleSheet, Platform } from 'react-native';
import { colors, fonts } from 'components/config';
import normalize from 'components/helpers/normalizeText';

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center'
  },
});
