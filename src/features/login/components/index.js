import React from 'react';
import { 
  View, 
  Text,
  Button,
} from 'react-native';
import styles from './styles';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

class Login extends React.Component {

  constructor(props){
    super(props);
  }

  static navigationOptions = {
    title: "Iniciar Sesión",
    header: null,
  };

  state = {
    data: {
      name: '',
      password: '',
    }
  }

  onChange = name => value => {
    this.setState({
      data: { ...this.state.data, [name]: value }
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <Text>Login View</Text>
        <Button
          title="Login"
          onPress={() => this.props.navigateToHome()}
        />
      </View>
    );
  }
}

export default Login;
