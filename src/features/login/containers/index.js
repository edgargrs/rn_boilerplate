import { connect } from 'react-redux';
import Component from 'features/login/components';
import { bindActionCreators } from 'redux';
import {
  navigateToHome,
  } from 'navigation/actions';

const mapStateToProps = state => ({

});

const mapDispatchToProps = dispatch => bindActionCreators({ 
  navigateToHome,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Component);