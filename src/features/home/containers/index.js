import { connect } from 'react-redux';
import Component from 'features/home/components';
import { bindActionCreators } from 'redux';
import {
  navigateToProfile
} from 'navigation/actions';

const mapStateToProps = state => ({

});

const mapDispatchToProps = dispatch => bindActionCreators({
  navigateToProfile,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Component);