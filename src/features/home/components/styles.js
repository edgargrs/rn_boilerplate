import { StyleSheet } from 'react-native';
import { colors } from 'components/config';
import normalize from 'components/helpers/normalizeText';
import { Dimensions } from "react-native";

const screenWidth = Math.round(Dimensions.get('window').width);
const newAfiliateWidth = screenWidth - (screenWidth * 0.20);

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
});
