import React from "react";
import { 
  View,
  Text,
  Button,
 } from 'react-native';
import styles from './styles';

class HomeScreen extends React.Component {

  static navigationOptions = {
    gesturesEnabled: false,
    header: null,
  };

  render() {
    return (
      <View style={styles.container}>
        <Text>Home screen</Text>
        <Button title="Perfil" onPress={() => this.props.navigateToProfile()} />
      </View>
    );
  }
}






export default HomeScreen;
