import React, { Component } from "react";
import PropTypes from "prop-types";
import Navigation from "navigation/containers";

class AppContainer extends Component {
  static propTypes = {
    startup: PropTypes.func.isRequired
  };

  componentDidMount() {
    this.props.startup();
  }

  render() {
    return ( <Navigation />);
  }
}
export default AppContainer;
