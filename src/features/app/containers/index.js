import { connect } from "react-redux";
import Component from "features/app/components";
import { startup } from "features/app/actions";

const mapDispatchToProps = dispatch => ({
  startup: () => dispatch(startup())
});

export default connect(
  null,
  mapDispatchToProps
)(Component);
