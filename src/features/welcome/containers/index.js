import { connect } from 'react-redux';
import Component from 'features/welcome/components';
import { bindActionCreators } from 'redux';
import {
  navigateToLogin,
  navigateToSignup,
  navigateToTerms
  } from 'navigation/actions';

const mapStateToProps = state => ({

});

const mapDispatchToProps = dispatch => bindActionCreators({ 
    navigateToLogin, navigateToSignup, navigateToTerms
  }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Component);