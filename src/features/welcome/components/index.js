import React from 'react';
import { View, Text, Button,Image } from 'react-native';
import styles from './styles';
import Images from 'assets/images.js';

class Welcome extends React.Component {

  constructor(props){
    super(props);
  }

  static navigationOptions = {
    header: null
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.logoContainer}>
          <Text>Logotipo</Text>
        </View>
        <Button
          title="Iniciar Sesión"
          onPress={ () => this.props.navigateToLogin() }
        />
        <Button
          title="Registrarse"
          onPress={ () => this.props.navigateToSignup() }
        />
        <Text style={styles.terms} onPress={() => this.props.navigateToTerms() }>Terminos y condiciones</Text>
      </View>
    );
  }
}

export default Welcome;
