import { applyMiddleware, createStore, compose } from 'redux';
import { persistReducer, persistStore } from 'redux-persist';
import { enableBatching } from 'redux-batched-actions';
import thunk from 'redux-thunk';
import createSagaMiddleware from 'redux-saga';
import { middleware as navigationMiddleware } from './navigation/utils/redux';
import { REDUX_PERSIST, DebugConfig } from 'config';
import appReducer from './reducers';
import appSaga from './sagas';
import Reactotron from './config/ReactotronConfig'

const sagaMiddleware = createSagaMiddleware();

const configureStore = () => {
    const middlewares = [sagaMiddleware, thunk, navigationMiddleware];
    const enhancers = DebugConfig.useReactotron ? [applyMiddleware(...middlewares), Reactotron.createEnhancer()] : [applyMiddleware(...middlewares)];

    const persistedReducer = persistReducer(REDUX_PERSIST, appReducer);

    const store = createStore(
        enableBatching(persistedReducer),
        appReducer(undefined, {}),
        compose(...enhancers)
    );

    const persistor = persistStore(store);

    sagaMiddleware.run(appSaga);

    return { store, persistor };
}

export default configureStore;