import colors from "./colors";
import {FONT_GUIDELINE as fonts} from "./fonts";

export { colors, fonts };
