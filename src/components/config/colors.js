export default {
    primary: '#F1833B',
    secondary: '#2674FF',
    black: '#000000',
    white: '#FFFFFF',
    error: '#ff190c',
  };
  