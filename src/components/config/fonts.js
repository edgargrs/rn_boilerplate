import { StyleSheet, Platform, } from 'react-native';
import normalize from 'components/helpers/normalizeText';

export const FONT_VARIANT = StyleSheet.create({
  regular: {
    fontFamily:  Platform.OS === 'ios' ? 'Futura' : 'sans-serif'
  },
  medium: {
    fontFamily:  Platform.OS === 'ios' ? 'Futura-Medium' : 'sans-serif-medium'
  }
});

export const FONT_GUIDELINE = StyleSheet.create({
  bigfont: {
    ...StyleSheet.flatten([FONT_VARIANT.regular]),
    fontSize: normalize(30)
  },
  title: {
    ...StyleSheet.flatten([FONT_VARIANT.regular]),
    fontSize: normalize(28)
  },
  input: {
    ...StyleSheet.flatten([FONT_VARIANT.medium]),
    fontSize: normalize(18),
  },
  input_regular: {
    ...StyleSheet.flatten([FONT_VARIANT.regular]),
    fontSize: normalize(18),
    fontWeight: '100'
  },
});
